// handle change é responsável por captar e armazenar o state
/* handle submit pega as informações do estado, e pode trabalhá-las, inclusive passando-as
para middlewares ou outras funções.
*/

import React from "react";
import styles from './buy.module.scss'
import Token from '../ethereum/build/Token.json'
import EthSwap from '../ethereum/build/EthSwap.json'
import web3 from '../ethereum/web3'
import But2 from './ButtonSell.js'
import ButSell from "./ButtonSell2"
//import {Spinner} from '../components/Spinner'
// não precisa usar useEffect se não for chamada assíncrona

import {
  useWeb3React
} from "@web3-react/core";

const networkID = 1;

export default function SellForm() {

  let tokenData = Token.networks[networkID].address
  const addressToken = tokenData
  const ABI = Token.abi

  const [token, setToken] = React.useState(undefined);
  const [account, setAccount] = React.useState('');
  const [tokenBalance, setTokenBalance] = React.useState();
  const [ethBalance, setEthBalance] = React.useState(undefined);
  const [tokenAmount, setTokenAmount] = React.useState(undefined);
  React.useEffect(() => {

    const loadTokenData = async () => {
      await ethereum.enable();
      const accounts = await web3.eth.getAccounts();
      setAccount(accounts[0])
      const result2 = new web3.eth.Contract(ABI, addressToken);
      setToken(result2)
    }
    loadTokenData()
  }, []);

  // loadToken
  React.useEffect(() => {

    const loadTokenData = async () => {
      const result2 = new web3.eth.Contract(ABI, addressToken);
      setToken(result2)
    }
    loadTokenData()
  }, []);

  console.log(token)
  console.log(account)

  // load ethswapData
  let ethSwapData = EthSwap.networks[networkID].address
  const addressEthSwap = ethSwapData
  const ETHABI = EthSwap.abi

  const [ethSwap, setEthSwapContract] = React.useState()

  console.log(ethSwap)

  React.useEffect(() => {
    const loadEthSwapData = async () => {
      const result2 = new web3.eth.Contract(ETHABI, addressEthSwap);
      setEthSwapContract(result2)
    }
    loadEthSwapData()
  }, []);
  // o nome do parâmetro ele chama de token amount, mas o valor passado é etherAmount

  const sellTokens = (tokenAmount) => {

    token.methods.approve(addressEthSwap, tokenAmount).send({ from: account }).on('transactionHash', (hash) => {
      let eswap = ethSwap.methods.sellTokens(tokenAmount).send({ from: account }).on('transactionHash', (hash) => {


      })
    })
  }
  // handleSubmit
  const handleSubmit = async (e) => {
    e.preventDefault();
    let result
    result = etherAmount.toString()
    let r = web3.utils.toWei(result, 'ether')
    sellTokens(r)
  }

  // handleChange

  const [etherAmount, setEtherAmount] = React.useState('');
  const handleChange = async (e) => {
    e.preventDefault();
    const ether2 = e.target.value * 1

    setEtherAmount(ether2)


  }
  // ethAmount is the user value input


  return (


    <form onSubmit={handleSubmit} className={styles.back}>

      <span className={styles.swap}>
        Swap
      </span>

      <div className={styles.inputContainer}>
        <But2 />
        <input
          type="text"
          className="form-control form-control-lg" style={{
            borderRadius: "20px", height: "100px",
            backgroundColor: "#151719", color: "white"
          }}
          placeholder="0.0"
          required
          onChange={handleChange}
        />

      </div>


      <div className={styles.inputContainer}>
        <ButSell />
        <input readOnly
          className="form-control form-control-lg" style={{ borderRadius: "20px", height: "100px", backgroundColor: "#151719", color: "white" }}
          type="text"
          value={etherAmount}
          placeholder="0.0"

        />


      </div>

      <div className="mb-1">
        <span className="float-left text-muted"></span>
        <span className="float-right text-muted"></span>
      </div>


      <button type="submit" className="btn btn-success btn-block btn-lg"
        style={{ borderRadius: "50px", height: "50px", marginTop: "30px", width: "470px", marginBottom: "1px" }}

      >Enter an amount</button>
    </form>


  )
}
