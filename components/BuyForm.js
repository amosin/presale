
// handle change é responsável por captar e armazenar o state
/* handle submit pega as informações do estado, e pode trabalhá-las, inclusive passando-as
para middlewares ou outras funções.
*/

import React from "react";
import styles from './buy.module.scss'
import Token from '../ethereum/build/Token.json'
import EthSwap from '../ethereum/build/EthSwap.json'
import web3 from '../ethereum/web3'
import But from './Button'
import But2 from './Button2'
//import {Spinner} from '../components/Spinner'
// não precisa usar useEffect se não for chamada assíncrona

const networkID = 1;


if (networkID === 97 || networkID === 56) {
  var currentRate = 17;
  var usdToken = "binancecoin";
} else if (networkID === 1 || networkID === 4) {
  var currentRate = 128;
  var usdToken = "ethereum";
}

console.log(usdToken)

import {
  useWeb3React
} from "@web3-react/core";

export default function BuyForm({ buttonSell }) {

  let tokenData = Token.networks[networkID].address
  const addressToken = tokenData
  const ABI = Token.abi

  const [token, setToken] = React.useState(undefined);
  const [account, setAccount] = React.useState('');
  const [tokenBalance, setTokenBalance] = React.useState();
  const [ethBalance, setEthBalance] = React.useState(undefined);

  React.useEffect(() => {

    const loadTokenData = async () => {
      if (window.web3) {
        await ethereum.enable();
        const accounts = await web3.eth.getAccounts();
        setAccount(accounts[0])
        const result2 = new web3.eth.Contract(ABI, addressToken);
        setToken(result2)
        const result3 = await result2.methods.balanceOf(accounts[0]).call();
        setTokenBalance(web3.utils.fromWei(result3.toString(), 'Ether'))
      }
    }
    loadTokenData()
  }, []);

  // loadToken
  // React.useEffect(() => {
  //
  //   const loadTokenData = async () => {
  //     const result2 = new web3.eth.Contract(ABI, addressToken);
  //     setToken(result2)
  //   }
  //   loadTokenData()
  // }, []);

  // console.log("Token: " + token)
  // console.log("Account: " + account)
  // console.log("You have this much IAUD: " + tokenBalance)


  // load ethswapData
  let ethSwapData = EthSwap.networks[networkID].address
  const addressEthSwap = ethSwapData
  const ETHABI = EthSwap.abi

  const [userChainID, setUserChainID] = React.useState()
  const [ethSwap, setEthSwapContract] = React.useState()
  const [rate, setRate] = React.useState(undefined);

  //console.log(ethSwap)

  React.useEffect(() => {
    const loadEthSwapData = async () => {
      const userChainID = await web3.eth.getChainId()
      setUserChainID(userChainID)
      const result2 = new web3.eth.Contract(ETHABI, addressEthSwap);
      setEthSwapContract(result2)
      if (window.web3) {
        if (userChainID != networkID) {
          setRate(currentRate)
        } else {
          const contractRate = await result2.methods.rate().call();
          setRate(contractRate)
        }
      } else {
        setRate(currentRate)
      }
    }
    loadEthSwapData()
  }, []);

  const [usdPrice, setUsdPrice] = React.useState(undefined);
  const [brlPrice, setBrlPrice] = React.useState(undefined);
  // Get prices in Fiat
  React.useEffect(() => {
    const getFiatPrices = async () => {
      const coinGeckoPrice = await fetch(`https://api.coingecko.com/api/v3/simple/price?ids=${usdToken}&vs_currencies=brl%2Cusd`)
      const priceJson = await coinGeckoPrice.json()
      // console.log(priceJson.ethereum.brl)
      if (usdToken === "binancecoin") {
        setUsdPrice(priceJson.binancecoin.usd)
        setBrlPrice(priceJson.binancecoin.brl)
      } else if (usdToken === "ethereum") {
        setUsdPrice(priceJson.ethereum.usd)
        setBrlPrice(priceJson.ethereum.brl)
      }

    }
    getFiatPrices()
  }, []);


  const buyTokens = (etherAmount) => {
    // this.setState({ loading: true })

    let eswap = ethSwap.methods.buyTokens().send({ value: etherAmount, from: account }).on('transactionHash', (hash) => {

      //   this.setState({ loading: false })
    })
  }
  // handleSubmit
  const handleSubmit = async (e) => {
    e.preventDefault();
    let result = etherAmount.toString()
    let r = web3.utils.toWei(result, 'ether')
    buyTokens(r)
  }

  // handleChange

  const [etherAmount, setEtherAmount] = React.useState('');
  const handleChange = async (e) => {
    e.preventDefault();
    const ether2 = e.target.value * 1
    setEtherAmount(e.target.value)
    //console.log(etherAmount)

  }
  // ethAmount is the user value input


  return (
    <>
      <div style={
        {
          color: "white",
          height: "50px",
          position: "absolute",
          marginTop: "-60px",
          marginBottom: "1px"
        }}>

        Your Balance: <b>{(tokenBalance) ? tokenBalance : 0}</b> <b>IAUD</b>

      </div>
      <div style={
        {
          color: "white",
          textAlign: "right",
          height: "50px",
          position: "absolute",
          marginTop: "-100px",
          marginLeft: "370px",
          marginBottom: "1px"
        }}>

        {userChainID != networkID ?
          <h6 style={{ fontSize: 20, textAlign: "right", color: "red" }}>WRONG NETWORK!</h6> :
          <p><b>IAUD</b>   PRICE <br />
            USD: ${(usdPrice / rate).toFixed(2)} <br /> BRL: ${(brlPrice / rate).toFixed(2)}</p>
        }
      </div>
      <form onSubmit={handleSubmit} className={styles.back}>

        <span className={styles.swap}>
          Instruaud Token Swap
        </span>
        <span style={{ fontSize: 20, textAlign: "right", color: "yellow" }}>
          {usdToken === "binancecoin" ?
            <h6>

              <div className={styles.dropdown}>
                <button className={styles.dropbtn}> <img src="eth/images/bnblogo.png" alt="bnb"
                  style={{
                    width: "30px", height: "30px",
                    justifyContent: "left"
                  }} /> Binance Smart Chain</button>
                <div className={styles.dropdownContent}>
                  <a href="https://presale.instruaud.com/eth"> <img src="eth/images/eth.png" alt="bnb"
                    style={{
                      width: "30px", height: "30px",
                      justifyContent: "left"
                    }} /> Ethereum</a>
                </div>
              </div>

            </h6>
            : <h6>
              <div className={styles.dropdown}>
                <button className={styles.dropbtn}><img src="eth/images/eth.png" alt="bnb"
                  style={{
                    width: "30px", height: "30px",
                    justifyContent: "left"
                  }} /> Ethereum</button>
                <div className={styles.dropdownContent}>
                  <a href="https://presale.instruaud.com/bsc"> <img src="eth/images/bnblogo.png" alt="bnb"
                    style={{
                      width: "30px", height: "30px",
                      justifyContent: "left"
                    }} /> Binance Smart Chain</a>
                </div>
              </div>

            </h6>
          }
        </span>
        <div className={styles.inputContainer}>
          <But />
          <input
            type="text"
            className="form-control form-control-lg" style={{
              borderRadius: "20px", height: "100px",
              backgroundColor: "#000", color: "white"
            }}
            placeholder="0.0"
            required
            onChange={handleChange}
          />

        </div>


        <div className={styles.inputContainer}>
          <But2 />
          <input readOnly
            className="form-control form-control-lg" style={{ borderRadius: "20px", height: "100px", backgroundColor: "#000", color: "white" }}
            type="text"
            value={etherAmount * rate}
            placeholder="0.0"

          />


        </div>

        <div className="mb-1">
          <span className="float-left text-muted"></span>
          <span className="float-right text-muted"></span>
        </div>


        <button type="submit" className="btn btn-success btn-block btn-lg"
          style={{ borderRadius: "50px", height: "50px", marginTop: "30px", width: "100%", marginBottom: "1px" }}

        >Enter an amount</button>
      </form>

    </>
  )
}
