import web3 from './web3';
import Token from './build/Token.json';

// dados do contrato. Precisa apenas do endereço e do ABI
const instance = new web3.eth.Contract(
  JSON.parse(Token.abi),
  '0x48e866e672E3b1A05D3A1caDE68e489028196127'
);

export default instance;
