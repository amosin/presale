import web3 from './web3';
import Token from './build/Token.json';

// dados do contrato. Precisa apenas do endereço e do ABI
const instance = new web3.eth.Contract(
  JSON.parse(Token.abi),
  '0xff5BF2Ff48a903a31519be152A6B881Ce3751d51'
);

export default instance;
