const Token = artifacts.require("Token");
const EthSwap = artifacts.require("EthSwap");

module.exports = async function (deployer) {

  // Deploy Token
  // await deployer.deploy(Token);
  // const token = await Token.deployed()

  const token = await Token.at('0xcE3F1c9f2fd7E8edCc38Ab0414fBC7292e7D372F')

  // Deploy EthSwap
  //await deployer.deploy(EthSwap, token.address);
  //const ethSwap = await EthSwap.deployed()

  const ethSwap = await EthSwap.at('0xEb0a6cca27C03d1ED537E248872138c9D868686f')

  // Transfer all tokens to EthSwap (1 million)
  // os tokens se transferiram
  //await token.transfer(ethSwap.address, '500000000000000000000000')

  // make ethSwap authorized minter
  // await token.addAuthorized(ethSwap.address);
  //await token.pause();

};
