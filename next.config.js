/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
	basePath: '/eth',
}

module.exports = nextConfig
